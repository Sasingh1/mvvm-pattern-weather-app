//
//  AddCityViewModel.swift
//  Weather
//
//  Created by Sartaj Singh on 30/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

class AddCityViewModel {
    
    let cityListModel: Dynamic<[CityListModel]>
    let isFinished: Dynamic<Bool>
    var onErrorHandling: ((ErrorResult?) -> Void)?
    
    private let cityListHandler: CityListHandler!
    
    init(withCityListHandler cityListHandler: CityListHandler = CityListHandler()) {
        self.cityListHandler = cityListHandler        
        self.cityListModel = Dynamic([])
        self.isFinished = Dynamic(false)
        self.fetchCityInfo()
    }
    
    private func fetchCityInfo() {
        DispatchQueue.global(qos: .userInteractive).async {
            self.cityListHandler.fetchCityInfo(withfileName: "citylist") { [weak self] result in
                DispatchQueue.main.async {
                    self?.isFinished.value = true
                    switch result {
                    case .success(let list) :
                        self?.cityListModel.value = list
                        break
                    case .failure(let error) :
                        print("Parser error \(error)")
                        self?.onErrorHandling?(error)
                        break
                    }
                }
            }
        }
    }
}
