//
//  WeatherListViewModel.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

class WeatherListViewModel {
    
    let weatherList: Dynamic<[WeatherInformation]>
    let isFinished: Dynamic<Bool>
    var onErrorHandling: ((ErrorResult?) -> Void)?
    
    let weatherListHandler = WeatherListHandler()
    private var cityList: [CityListModel]!
    
    private var periodicTimer: Timer!
    private let timePeriod = 60 * 10 //10
    
    
    init() {
        self.weatherList = Dynamic([])
        self.isFinished = Dynamic(false)
        
        periodicTimer = Timer.scheduledTimer(timeInterval: TimeInterval(timePeriod),target: self, selector: #selector(runTimeCode), userInfo: nil, repeats: true)
        self.fetchCityInfo()
    }
    
    @objc private func runTimeCode(_ sender: AnyObject) {
        self.fetchCityInfo()
    }
    
    func pullToRefresh() {
        self.fetchCityInfo()
    }
    
    private func fetchCityInfo() {
        self.weatherListHandler.fetchCityInfo(withFileName: "StartCity") { [weak self] result in
            switch result {
            case .success(let list) :
                self?.cityList = list
                self?.fetchWeatherInfo(byCityList: list)
                break
            case .failure(let error) :
                self?.isFinished.value = true
                self?.onErrorHandling?(error)                
         }
       }
    }
    
    private func fetchWeatherInfo(byCityList cityList:[CityListModel]) {
        let arrayId = cityList.map { String($0.id!) }
        let cityIDs = arrayId.joined(separator: ",")
        
        self.weatherListHandler.fetchWeatherInfo(withCityIDs: cityIDs) { [weak self] result in
            DispatchQueue.main.async {
                self?.isFinished.value = true
                switch result {
                case .success(let list) :
                    self?.weatherList.value = list
                    break
                case .failure(let error) :
                    print("Parser error \(error)")
                    self?.onErrorHandling?(error)
                    break
                }
            }
        }
    }
    
    func fetchWeatherInfo(byCity city: CityListModel) {
        let results = self.cityList.filter { $0.name == city.name && $0.id == city.id }
        let isExists = results.isEmpty == false
        if
            let cityId = city.id,
            !isExists
        {
            self.isFinished.value = false
            self.weatherListHandler.fetchWeatherInfo(withCityID: "\(cityId)") { [weak self] result in
                var list = self?.weatherList.value
                DispatchQueue.main.async {
                    self?.isFinished.value = true
                    switch result {
                    case .success(let info) :
                        list?.append(info)
                        self?.weatherList.value = list ?? [info]
                        break
                    case .failure(let error) :
                        print("Parser error \(error)")
                        self?.onErrorHandling?(error)
                        break
                    }
                }
            }
        }
    }
}
