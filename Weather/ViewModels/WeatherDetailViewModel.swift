//
//  WeatherDetailViewModel.swift
//  Weather
//
//  Created by Sartaj Singh on 30/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

class WeatherDetailViewModel {
    
    let dataSource: Dynamic<[[DetailModel]]>
    var onErrorHandling: ((ErrorResult?) -> Void)?
    
    private let weatherDetailHandler: WeatherDetailHandler!
    private let weatherInfo: WeatherInformation?
    
    init(withCityListHandler weatherDetailHandler: WeatherDetailHandler = WeatherDetailHandler(), withWeatherInformation weatherInfo: WeatherInformation?) {
        self.weatherDetailHandler = weatherDetailHandler
        self.weatherInfo = weatherInfo
        
        self.dataSource = Dynamic([[]])
        self.fetchCityInfo()
    }
    
    private func fetchCityInfo() {
        if let weatherInfo = self.weatherInfo {
            self.weatherDetailHandler.fetchWeatherInfo(withWeatherInfo: weatherInfo) { [weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let info):
                        self?.dataSource.value = info
                    case .failure(let error):
                        self?.onErrorHandling?(error)
                    }
                }
            }
        } else {
            self.onErrorHandling?(ErrorResult.custom(string: "WeatherInformation is missing"))
        }
    }
}
