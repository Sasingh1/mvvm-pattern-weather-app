//
//  UIColor.swift
//  Weather
//
//  Created by Sartaj Singh on 29/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit


extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIColor {
    static var viewBackgroundColor: UIColor {
        return UIColor(rgb: 0xeef0f1)
    }
    static var collectionViewBackgroundColor: UIColor {
        return UIColor(rgb: 0xEAE8EA)
    }
    static var tableViewBackgroundColor: UIColor {
        return UIColor(rgb: 0xEAE8EA)
    }
}
