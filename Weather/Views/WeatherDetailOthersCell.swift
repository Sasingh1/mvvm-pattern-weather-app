//
//  WeatherDetailOthersCell.swift
//  Weather
//
//  Created by Sartaj Singh on 30/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit

class WeatherDetailOthersCell: UICollectionViewCell {
    @IBOutlet weak var bagroundView: UIView!
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var detailLabel: UILabel?
    @IBOutlet var weatherImage: UIImageView?
    
    var detailModel: DetailModel? {
        didSet {
            guard let data = detailModel else {
                return
            }
            titleLabel?.text = data.title
            detailLabel?.text = data.description
            weatherImage?.image = UIImage(named: data.image!)
        }
    }
}
