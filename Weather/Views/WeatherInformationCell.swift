//
//  WeatherInformationCell.swift
//  Weather
//
//  Created by Sartaj Singh on 29/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit

class WeatherInformationCell: UITableViewCell {

    @IBOutlet weak var labelCityName: UILabel!
    @IBOutlet weak var labelCityTemperature: UILabel!
    
    var WeatherModel: WeatherInformation? {
        didSet {
            guard let data = WeatherModel else {
                return
            }
            labelCityName.text = data.name
            labelCityTemperature.text = "\(data.main?.temp ?? 0) °C"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
