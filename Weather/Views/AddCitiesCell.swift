//
//  AddCitiesCell.swift
//  Weather
//
//  Created by Sartaj Singh on 30/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit

class AddCitiesCell: UITableViewCell {

    @IBOutlet weak var labelCityName: UILabel!
    @IBOutlet weak var labelCityId: UILabel!
    
    var addCitiesModel: CityListModel? {
        didSet {
            guard let data = addCitiesModel else {
                return
            }
            labelCityName.text = data.name
            labelCityId.text = "\(data.id ?? 0)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
