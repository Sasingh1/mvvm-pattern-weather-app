//
//  WeatherDetailTemperatureCell.swift
//  Weather
//
//  Created by Sartaj Singh on 30/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit

class WeatherDetailTemperatureCell: UICollectionViewCell {
    @IBOutlet weak var bagroundView: UIView!
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var detailLabel: UILabel?
    
    var detailModel: DetailModel? {
        didSet {
            guard let data = detailModel else {
                return
            }
            titleLabel?.text = data.title
            detailLabel?.text = data.description
        }
    }
}
