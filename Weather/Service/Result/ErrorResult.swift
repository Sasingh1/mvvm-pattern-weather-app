//
//  ErrorResult.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}
