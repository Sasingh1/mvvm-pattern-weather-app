//
//  WebService.swift
//  Weather
//
//  Created by Sartaj Singh on 28/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct Resource<T> {
    let url: URL
    let parse: (Data) -> T?
}

final class WebService {
    func load<T>(resource: Resource<T>, completion: @escaping (T?) -> Void) {
        URLSession.shared.dataTask(with: resource.url) {data,_,_ in
            if let data = data  {
                DispatchQueue.main.async {
                    completion(resource.parse(data))
                }
            } else {
                    completion(nil)
            }
        }.resume()
    }
}
