//
//  APIManager.swift
//  Weather
//
//  Created by Sartaj Singh on 28/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation


struct APIManager {
    private static let baseURLString = "http://api.openweathermap.org/data/2.5/"
    private static let apiKey = "5a230a39a381b73ad15ca01075d63117"
    private static let groupUnit = "group?"
    private static let weatherUnit = "weather?"
    
    private static let Metric = "metric"
    
    public static func weatherAPIURL(_ cityID: String) -> URL {
        let weatherInfoUrl =  baseURLString + weatherUnit + "id=\(cityID)&units=\(Metric)&APPID=\(apiKey)"
        let finalURL = URL(string: weatherInfoUrl)!
        return finalURL
    }
    
    public static func weatherGroupAPIURL(_ cityID: String) -> URL {
        let weatherInfoUrl =  baseURLString + groupUnit + "id=\(cityID)&units=\(Metric)&APPID=\(apiKey)"
        let finalURL = URL(string: weatherInfoUrl)!
        return finalURL
    }
}
