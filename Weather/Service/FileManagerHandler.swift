//
//  FileManagerResource.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct FileManagerResource<T> {
    let fileName: String
    let parse: (Data) -> T?
}

class FileManagerHandler {
    init() {
    }
    
    func load<T>(resource: FileManagerResource<T>, completion: @escaping (T?) -> Void) {
        if let url = Bundle.main.url(forResource: resource.fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                completion(resource.parse(data))
                
            } catch (let error) {
                print("error:\(error)")
                completion(nil)
            }
        }
    }
}
