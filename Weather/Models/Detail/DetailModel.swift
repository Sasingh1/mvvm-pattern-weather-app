//
//  DetailModel.swift
//  Weather
//
//  Created by Sartaj Singh on 30/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct DetailModel {
    let title: String?
    let description: String?
    let image: String?
}
