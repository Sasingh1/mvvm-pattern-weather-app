//
//  WeatherMapResult.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct WeatherMapResult: Codable {
    var cnt: Int?
    var list: [WeatherInformation]?
}
