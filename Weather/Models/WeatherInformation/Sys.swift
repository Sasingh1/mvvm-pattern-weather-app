//
//  Sys.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct Sys: Codable {
    var sunset: Int?
    var sunrise: Int?
    var message: Double?
    var id: Int?
    var type: Int?
    var country: String?
}
