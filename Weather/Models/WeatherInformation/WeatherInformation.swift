//
//  WeatherInformation.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct WeatherInformation: Codable {
    var main: Main?
    var name: String?
    var id: Int?
    var coord: Coord?
    var weather: [Weather]?
    var clouds: Clouds?
    var dt: Int?
    var base: String?
    var sys: Sys?
    var cod: Int?
    var visibility: Int?
    var wind: Wind?    
}
