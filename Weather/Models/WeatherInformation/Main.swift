//
//  Main.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct Main: Codable {
    var temp: Double?
    var pressure: Double?
    var humidity: Int?
    var temp_min: Double?
    var temp_max: Double?
}
