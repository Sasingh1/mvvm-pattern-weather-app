//
//  Wind.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct Wind: Codable {
    var deg: Double?
    var speed: Double?
}
