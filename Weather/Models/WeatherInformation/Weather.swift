//
//  Weather.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct Weather: Codable {
    var main: String?
    var icon: String?
    var description: String?
    var id: Int?
}
