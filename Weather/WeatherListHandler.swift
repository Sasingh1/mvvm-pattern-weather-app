//
//  WeatherListHandler.swift
//  Weather
//
//  Created by Sartaj Singh on 27/7/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

class WeatherListHandler {
    
    private let fileManagerHandler: FileManagerHandler = FileManagerHandler()
    private let webService: WebService = WebService()
    
    func fetchCityInfo(withFileName fileName: String, completion: @escaping ((Result<[CityListModel], ErrorResult>) -> Void)) {
        
        let cityResource = FileManagerResource<[CityListModel]>(fileName: "StartCity") { data in
            let cityListModel = try? JSONDecoder().decode([CityListModel].self, from: data)
            return cityListModel
        }
        
        self.fileManagerHandler.load(resource: cityResource) { response in
            if let result = response {
                completion(.success(result))
            } else {
                completion(.failure(.parser(string: "Error while parsing json data")))
            }
            
        }
    }
    
    func fetchWeatherInfo(withCityIDs cityIDs: String, completion: @escaping ((Result<[WeatherInformation], ErrorResult>)) -> Void ) {
        
        let weatherURL = APIManager.weatherGroupAPIURL(cityIDs)
        let weatherResource = Resource<WeatherMapResult>(url: weatherURL) { data in
            let weatherInformationModel = try? JSONDecoder().decode(WeatherMapResult.self, from: data)
            return weatherInformationModel
        }
        
        self.webService.load(resource: weatherResource) { response in
            if
                let result = response,
                let weatherInfo = result.list
            {
                    completion(.success(weatherInfo))
            } else {
                completion(.failure(.parser(string: "Error while parsing json data")))
            }
        }
    }
    
    func fetchWeatherInfo(withCityID cityID: String, completion: @escaping ((Result<WeatherInformation, ErrorResult>) -> Void)) {
        let weatherURL = APIManager.weatherAPIURL(cityID)
        let cityResource = Resource<WeatherInformation>(url: weatherURL) { data in
            let cityListModel = try? JSONDecoder().decode(WeatherInformation.self, from: data)
            return cityListModel
        }
        
        self.webService.load(resource: cityResource) { response in
            if let result = response {
                completion(.success(result))
            } else {
                completion(.failure(.parser(string: "Error while parsing json data")))
            }
        }
    }
}
